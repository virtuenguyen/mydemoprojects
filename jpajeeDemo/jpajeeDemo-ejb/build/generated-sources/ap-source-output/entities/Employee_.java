package entities;

import entities.Company;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-17T16:12:10")
@StaticMetamodel(Employee.class)
public class Employee_ { 

    public static volatile SingularAttribute<Employee, String> phone;
    public static volatile SingularAttribute<Employee, String> name;
    public static volatile SingularAttribute<Employee, Company> company;
    public static volatile SingularAttribute<Employee, Integer> id;

}