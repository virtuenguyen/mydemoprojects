package entities;

import entities.Employee;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-17T16:12:10")
@StaticMetamodel(Company.class)
public class Company_ { 

    public static volatile ListAttribute<Company, Employee> employeeList;
    public static volatile SingularAttribute<Company, String> name;
    public static volatile SingularAttribute<Company, String> adresss;
    public static volatile SingularAttribute<Company, Integer> id;
    public static volatile SingularAttribute<Company, Date> createDate;

}