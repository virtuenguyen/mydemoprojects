/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aopspringexample.models;

/**
 * @author virtu
 */
public class person {
    private String name;
    private String age;

    public void SetNewPerson(String Name, String Age){
        this.name = Name;
        this.age = Age;
    }
    
    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(String age) {
        this.age = age;
    }
    
    
 }
