/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aopspringexample.models;

/**
 *
 * @author virtu
 */
public class customer extends person{
    
    private String cus_Class;
    private String cus_location;

    public String getCus_Class() {
        return cus_Class;
    }

    public String getCus_location() {
        return cus_location;
    }

    public void setCus_Class(String cus_Class) {
        this.cus_Class = cus_Class;
    }

    public void setCus_location(String cus_location) {
        this.cus_location = cus_location;
    }
    
    
}
