/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aopspringexample.controllers;

import com.mycompany.aopspringexample.aspects.Loggable;
import com.mycompany.aopspringexample.models.*;

/**
 *
 * @author virtu
 */
public class employeeController {
    
    employee currentEmployee;

    public employee getCurrentEmployee() {
        return currentEmployee;
    }

    public void setCurrentEmployee(employee currentEmployee) {
        this.currentEmployee = currentEmployee;
    }
    
    public void newPerson(String name, String age){
        currentEmployee = new employee();
        currentEmployee.SetNewPerson(name, age);
    }
    
    public void setEmployeeDepartment(String Department){
        currentEmployee.setDepartment(Department);
    }
    
    @Loggable
    public void setEmployeeSalary(double Salary){
        currentEmployee.setSalary(Salary);
    }
    
    public String getEmployeeInfo (){
        return "Employee Infomation -     name: " + currentEmployee.getName() + "  age:" + currentEmployee.getAge(); 
    }
    
    public String getEmployeeDepartment(){
        return currentEmployee.getDepartment();
    }
    
    public double getEmployeeSalary(){
        return currentEmployee.getSalary();
    }
    
}
