/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aopspringexample.controllers;

import com.mycompany.aopspringexample.models.customer;

/**
 * @author virtue
 */
public class customerController {
    
    customer currentCustomer;

    public customer getCurrentCustomer() {
        return currentCustomer;
    }

    public void setCurrentCustomer(customer currentCustomer) {
        this.currentCustomer = currentCustomer;
    }
    
    public void newPerson(String name, String age){
        currentCustomer = new customer();
        currentCustomer.SetNewPerson(name, age);
    }
    
    public void setCustomerLocation(String Location){
        currentCustomer.setCus_location(Location);
    }
    
    public void setCustomerClass(String Class){
        currentCustomer.setCus_Class(Class);
    }
    
    public String getCusomerInfo (){
        return "Customer Infomation -     name: " + currentCustomer.getName() + "  age:" + currentCustomer.getAge(); 
    }
    
    public String getCustomerClass(){
        return currentCustomer.getCus_Class();
    }
    
    public String getCustomerLocation(){
        return currentCustomer.getCus_location();
    }
}
