/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aopspringexample;

import com.mycompany.aopspringexample.controllers.employeeController;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/** @author virtu
 */
public class aopspringexample {
    public static void main (String[] args) {
        
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("springAOP.xml");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        
        //employeeController employeeService = (employeeController) ctx.getBean("newEmployeePersonAspect");
        employeeController employeeService = ctx.getBean("employeeService", employeeController.class);
        employeeService.newPerson("Nguyen Trong Duc", "35");
        employeeService.setEmployeeSalary(2000);
        
        System.out.println("Thong tin: " + employeeService.getEmployeeInfo());
        System.out.println("");
        System.out.println("");
        System.out.println("");
    }
}
