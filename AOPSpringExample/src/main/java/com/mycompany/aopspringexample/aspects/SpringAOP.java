/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aopspringexample.aspects;

import java.lang.reflect.Method;
import org.springframework.aop.MethodBeforeAdvice;

/**
 * @author virtu
 */
public class SpringAOP implements MethodBeforeAdvice{

    @Override public void before(Method method, Object[] os, Object o) throws Throwable {
        System.out.println("Spring AOP XMLConfig Aspect: Before invoking " + method.getName() + " method");
    }
}
