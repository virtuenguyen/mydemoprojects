/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aopspringexample.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 *
 * @author virtu
 */
@Aspect
public class employeeAnnotationAspect {
    @Before("@annotation(com.mycompany.aopspringexample.aspects.Loggable)")
    public void myAdvice(){
            System.out.println("Log : Employee's salary is updated !!");
    }
}
