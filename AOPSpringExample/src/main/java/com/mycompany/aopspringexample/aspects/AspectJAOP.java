package com.mycompany.aopspringexample.aspects;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author virtu
 */
@Aspect
public class AspectJAOP {               
    @AfterReturning(pointcut="execution(* *.getEmployeeInfo())", returning = "returnString" )
	public void getEmployeeInfoAdvice(String returnString){
		System.out.println("Get Employee Info Advice executed. Returned String = " + returnString);
	}
}
