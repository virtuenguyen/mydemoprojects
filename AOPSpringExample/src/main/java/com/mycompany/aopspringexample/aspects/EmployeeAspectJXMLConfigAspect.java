/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.aopspringexample.aspects;

/**
 * @author virtu
 */
public class EmployeeAspectJXMLConfigAspect {
     public void employeeGetSalaryAroundAdvice(){
        System.out.println("Employee AspectJ XML Config Aspect: Before invoking setSalary() method");
    }
}
